-- Solucion del proyecto final de Audacity
-- Swiss-system tournament

-- Borramos si existe la base de datos tournament
DROP DATABASE tournament;

CREATE DATABASE tournament;
\c tournament;


-- Creamos las tablas y views

CREATE TABLE player (
    id SERIAL PRIMARY KEY,
    name TEXT
);

CREATE TABLE match (
    player1_id INTEGER REFERENCES player (id),
    player2_id INTEGER REFERENCES player (id),
    winner_id INTEGER NULL REFERENCES player (id)
        CHECK (
            winner_id = player1_id
            or winner_id = player2_id
            or winner_id IS NULL
        ),
    PRIMARY KEY (player1_id, player2_id),
    CHECK (player1_id < player2_id)
);


CREATE VIEW win_count AS
    SELECT player.id as winner_id,
           COUNT(match.winner_id) as num_matches_won
    FROM player LEFT JOIN match
    ON player.id = match.winner_id
    GROUP BY player.id;


CREATE VIEW match_count AS
    SELECT player.id AS player_id,
           COUNT(player1_id) as num_matches
    FROM player LEFT JOIN match
    ON player.id = player1_id OR player.id = player2_id
    GROUP BY player.id;


CREATE VIEW player_standing AS
    SELECT winner_id as player_id,
           player.name,
           win_count.num_matches_won as win_count,
           match_count.num_matches as match_count
    FROM win_count
        JOIN player
            ON winner_id = player.id
        JOIN match_count
            ON winner_id = match_count.player_id
    ORDER BY win_count DESC;
